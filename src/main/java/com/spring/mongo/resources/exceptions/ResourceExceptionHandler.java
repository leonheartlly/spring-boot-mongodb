package com.spring.mongo.resources.exceptions;


import com.spring.mongo.services.exceptions.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice public class ResourceExceptionHandler {

        @ExceptionHandler(ObjectNotFoundException.class)
        public ResponseEntity<StandardError> objectNotFound( ObjectNotFoundException ex, HttpServletRequest http ) {

                HttpStatus status = HttpStatus.NOT_FOUND;
                StandardError standardError =
                    new StandardError( System.currentTimeMillis(), status.value(), "Não encontrado", ex.getMessage(), http.getRequestURI() );

                return ResponseEntity.status(status).body( standardError );
        }
}
