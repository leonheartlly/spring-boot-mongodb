package com.spring.mongo.resources;


import com.spring.mongo.domain.Post;
import com.spring.mongo.domain.User;
import com.spring.mongo.dto.UserDTO;
import com.spring.mongo.resources.util.URL;
import com.spring.mongo.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@RestController @RequestMapping( "/posts" ) public class PostsResource {

        @Autowired private PostService postService;

        @GetMapping( "/{id}" )
        public ResponseEntity<Post> findById( @PathVariable String id ) {

                Post post = postService.findById( id );

                return ResponseEntity.ok().body( post );
        }

        @RequestMapping( value="/titlesearch" )
        public ResponseEntity<List<Post>> findByTitle( @RequestParam(value="text", defaultValue = "") String text ) {

                text = URL.decodeParam(text);

                List <Post> posts = postService.findByWord( text );

                return ResponseEntity.ok().body( posts );
        }

        @RequestMapping( value="/fullsearch" )
        public ResponseEntity<List<Post>> fullSearch( @RequestParam(value="text", defaultValue = "") String text,
            @RequestParam(value="minDate", defaultValue = "") String minDate,
            @RequestParam(value="maxDate", defaultValue = "") String maxDate) {

                text = URL.decodeParam(text);
               Date min = URL.ConvertDate( minDate, new Date(0L) );
               Date max = URL.ConvertDate( maxDate, new Date() );

                List <Post> posts = postService.fullSearch( text, min, max );

                return ResponseEntity.ok().body( posts );
        }
}
