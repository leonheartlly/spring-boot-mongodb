package com.spring.mongo.resources;


import com.spring.mongo.domain.Post;
import com.spring.mongo.domain.User;
import com.spring.mongo.dto.UserDTO;
import com.spring.mongo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;


@RestController @RequestMapping( "/users" ) public class UserResource {

        @Autowired private UserService userService;


        @GetMapping public ResponseEntity<List<UserDTO>> findAll() {

                List<User> users = userService.findAll();

                List<UserDTO> listaDto = users.stream().map( x -> new UserDTO( x ) ).collect( Collectors.toList() );

                return ResponseEntity.ok().body( listaDto );
        }


        @GetMapping( "/{id}" )
        public ResponseEntity<UserDTO> findById( @PathVariable String id ) {

                User users = userService.findById( id );

                return ResponseEntity.ok().body( new UserDTO( users ) );
        }


        //        @PostMapping
        @RequestMapping( method = RequestMethod.POST )
        public ResponseEntity<Void> insert( @RequestBody UserDTO userDto ) {

                User user = userService.fromDTO( userDto );
                user = userService.insertUser( user );
                //Boa pratica retornar o caminho do spring que fez esta operação.
                //O código abaixo é padrão do spring
                URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path( "/{id}" ).buildAndExpand( user.getId() ).toUri();

                //created retorna código 201
                return ResponseEntity.created( uri ).build();

        }

        @RequestMapping( value="/{id}", method = RequestMethod.PUT )
        public ResponseEntity<Void> update( @RequestBody UserDTO userDto, @PathVariable String id  ) {

                User user = userService.fromDTO( userDto );
                //garantindo que o objeto tem o id informado
                user.setId( id );

                user = userService.update( user );

                //created retorna código 201
                return ResponseEntity.noContent().build();

        }

        @RequestMapping( value="/{id}", method = RequestMethod.DELETE )
        public ResponseEntity<Void> delete( @PathVariable String id ) {

                userService.delete( id );

                //Sem resposta deve se retornar o código 204, para isso é nocontent
                return ResponseEntity.noContent().build();
        }

        @GetMapping( "/{id}/posts" )
        public ResponseEntity<List<Post>> findUserPosts( @PathVariable String id ) {

                User users = userService.findById( id );

                return ResponseEntity.ok().body( users.getPosts() );
        }

}
