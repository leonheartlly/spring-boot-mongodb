package com.spring.mongo.services;


import com.spring.mongo.domain.Post;
import com.spring.mongo.domain.User;
import com.spring.mongo.dto.UserDTO;
import com.spring.mongo.repository.PostRepository;
import com.spring.mongo.repository.UserRepository;
import com.spring.mongo.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service public class PostService {

        @Autowired private PostRepository repository;



        public Post findById(String id){
               Optional<Post> posts = repository.findById( id );

               if(!posts.isPresent()){
                       throw new ObjectNotFoundException( "Objeto não encontrado." );
               }

               return posts.get();
        }


        public List<Post> findByWord(String word){

                //comentado para testar a query criada no @Query
//                List<Post> posts = repository.findByTitleContainingIgnoreCase( word );
                List<Post> posts = repository.findByTitle( word );

//                if(posts.isEmpty()){
//                        throw new ObjectNotFoundException( "Objeto não encontrado." );
//                }

                return posts;
        }

        public List<Post> fullSearch(String text, Date minDate, Date maxDate){

                //adicionando 24horas a data atual
                maxDate = new Date(maxDate.getTime() + (24 * 60 * 60 * 1000));
                return repository.fullSearch( text, minDate, maxDate );
        }


}
