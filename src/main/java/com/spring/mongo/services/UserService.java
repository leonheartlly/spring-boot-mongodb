package com.spring.mongo.services;


import com.spring.mongo.domain.User;
import com.spring.mongo.dto.UserDTO;
import com.spring.mongo.repository.UserRepository;
import com.spring.mongo.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service public class UserService {

        @Autowired private UserRepository repository;


        public List<User> findAll() {

                return repository.findAll();
        }

        public User findById(String id){
               Optional<User> user = repository.findById( id );

               if(!user.isPresent()){
                       throw new ObjectNotFoundException( "Objeto não encontrado." );
               }

               return user.get();
        }

        public User insertUser(User user){

                return repository.insert( user );
        }

        public void delete(String id){

                //Usado apenas para lançar excessão caso o id nao exista
                findById( id );

                repository.deleteById( id );
        }

        public User update(User newUser){

                User oldUser = findById( newUser.getId() );

                updateUser( newUser, oldUser );

                return repository.save( oldUser );

        }


        private void updateUser( User newUser, User oldUser ) {

                oldUser.setEmail( newUser.getEmail() );
                oldUser.setName( newUser.getName() );

        }


        public User fromDTO( UserDTO user){
                return new User(user.getId(), user.getName(), user.getEmail());
        }


}
