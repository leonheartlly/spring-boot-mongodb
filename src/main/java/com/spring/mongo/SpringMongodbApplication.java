package com.spring.mongo;


import com.spring.mongo.domain.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication public class SpringMongodbApplication {

        public static void main( String[] args ) {

                SpringApplication.run( SpringMongodbApplication.class, args );

        }

}

