package com.spring.mongo.repository;


import com.spring.mongo.domain.Post;
import com.spring.mongo.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository public interface PostRepository extends MongoRepository<Post, String> {

        List<Post> findByTitleContainingIgnoreCase( String word );

        //        Query padrão Mongodb
        //        @Query("{ <field>: {$regex: /pattern/, $options: '<options>' } } ")
        //        ?0 indica o primeiro parametro do método
        //        o options pode ser encontrado na lista de opçoes da documentação
        //        i indica ignore case
        @Query( "{ 'title': {$regex: ?0, $options: 'i' } } " )
        List<Post> findByTitle( String text );

        //Busca por um texto em qualquer lugar do comentario entre as datas

        /**
         * { $or: [ { <expression1> }, { <expression2> }, ... , { <expressionN> } ] } - ou
         * { $and: [ { <expression1> }, { <expression2> } , ... , { <expressionN> } ] } - and
         * {field: {$gte: value} } - maior que
         * { field: { $lte: value} } - menor que
         */
        @Query( "{ $and: [ {'date': {$gte: ?1} }, { 'date': { $lte: ?2} } , { $or: [ { 'title': {$regex: ?0, $options: 'i' } }, { 'body': {$regex: ?0, $options: 'i' } }, { 'coments.text': {$regex: ?0, $options: 'i' } } ] } ] }" )
        List<Post> fullSearch( String text, Date minDate, Date maxDate );
}
