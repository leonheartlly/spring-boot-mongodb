package com.spring.mongo.config;


import com.spring.mongo.domain.Post;
import com.spring.mongo.domain.User;
import com.spring.mongo.dto.AuthorDTO;
import com.spring.mongo.dto.ComentDTO;
import com.spring.mongo.repository.PostRepository;
import com.spring.mongo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.TimeZone;


@Configuration
public class Instantiation implements CommandLineRunner {

        @Autowired
        private UserRepository userRepository;

        @Autowired
        private PostRepository postRepository;

        @Override public void run( String... args )
            throws Exception {

                SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
                sdf.setTimeZone( TimeZone.getTimeZone( "GMT" ) );

                userRepository.deleteAll();

                User jonas = new User(null, "Jonas Bland", "Jonas@gmail.com");
                User gabriel = new User(null, "Gabriel Jesus", "jesus@macaco.com");
                User jurandir = new User(null, "Jurandir juraci", "jurandir@gmail.com");

                userRepository.saveAll( Arrays.asList( jonas, gabriel, jurandir ));
                postRepository.deleteAll();

                Post post = new Post(null, sdf.parse( "21/03/2018" ), "Partiu Natal", "Vou para Noronha!", new AuthorDTO( jonas ));
                Post post2 = new Post(null, sdf.parse( "23/10/2018" ), "Partiu Férias", "Vou sair de férias!", new AuthorDTO( jonas ));

                ComentDTO d1 = new ComentDTO( "Boa viagem aew!", sdf.parse( "21/03/2018" ), new AuthorDTO( gabriel ) );
                ComentDTO d2 = new ComentDTO( "Ai sim heim!", sdf.parse( "22/03/2018" ), new AuthorDTO( jurandir ) );
                ComentDTO d3 = new ComentDTO( "E aqueles relatórios que vc ficou de passar?!", sdf.parse( "23/10/2018" ), new AuthorDTO( gabriel ) );

                post.getComents().addAll( Arrays.asList( d1, d2 )) ;
                post2.getComents().add( d3 );

                postRepository.saveAll( Arrays.asList( post, post2 ) );

                jonas.getPosts().addAll( Arrays.asList( post, post2 ) );
                userRepository.save( jonas );

        }
}
