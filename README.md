## WS Spring Boot + Mongodb

Esta aplicação é um WS construido com Spring boot e mongodb.
O intuito é entender como funciona o mongodb e como integrar com spring.
Para rodar a aplicação será necessário que se instale o mongodb na maquina e se faça um start.

A classe **Instantiation** do projeto cria alguns inserts ao subir a aplicação para facilitar o teste de manipulação dos dados.

#### Algumas chamadas possíveis

localhost:8080/users - GET

localhost:8080/users/{id} - DELETE

localhost:8080/users/{id} - corpo {"name": "value", "email": "email@value"} - PUT

localhost:8080/posts/titlesearch?text=atal - GET

localhost:8080/posts/fullsearch?text=viagem&maxDate=2018-12-30 - GET

#### Documentação de consulta para criar queries com mongo

https://docs.mongodb.com/manual/reference/operator/query/

